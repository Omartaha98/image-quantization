using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ImageQuantization.Color_Pallete;
using ImageQuantization.Quantizer;
namespace ImageQuantization
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }
        ShowPallete p;
        RGBPixel[,] ImageMatrix;
        List<RGBPixel> li = new List<RGBPixel>();
        public static RGBPixel[, ,] RGB = new RGBPixel[256, 256, 256];
        public ImageQuantization.Quantizer.Quantizer quan;
        private void btnOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //Open the browsed image and display it
                string OpenedFilePath = openFileDialog1.FileName;
                ImageMatrix = ImageOperations.OpenImage(OpenedFilePath);
                li.Clear();
                li = getDistinct(ImageMatrix);
                ImageOperations.DisplayImage(ImageMatrix, pictureBox1);
                textBox1.Text = "";
                textBox2.Text = "";
                No_of_clust.Text = "";
                pictureBox2.Image = null;
            }
            txtWidth.Text = ImageOperations.GetWidth(ImageMatrix).ToString();
            txtHeight.Text = ImageOperations.GetHeight(ImageMatrix).ToString();
            textBox1.Text = li.Count.ToString();
        }

        private void btnGaussSmooth_Click(object sender, EventArgs e)
        {
            double sigma = double.Parse(txtGaussSigma.Text);
            int maskSize = (int)nudMaskSize.Value ;
            ImageMatrix = ImageOperations.GaussianFilter1D(ImageMatrix, maskSize, sigma);
            
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

            if (Auto_Detect.Checked == true)
            {
                // if auto detect with 0 or no number of cluster "" catch erorr"invalid numb cluster"
                No_of_clust.Text = "1";
                No_of_clust.Enabled = false;
            }
            else
                No_of_clust.Enabled = true;
        }
        private void Auto_Detect_CheckStateChanged(object sender, EventArgs e)
        {

            if (Auto_Detect.Checked == true)
            {
                // if auto detect with 0 or no number of cluster "" catch erorr"invalid numb cluster"
                No_of_clust.Text = "1";
                No_of_clust.Enabled = false;
            }
            else
                No_of_clust.Enabled = true;
        }

        private void Quantize_Click(object sender, EventArgs e)
        {
            int Clusters_No  = 0;
            if (No_of_clust.Text.ToString() != "") 
                Clusters_No = int.Parse(No_of_clust.Text);
            long after = 0;
            long before = 0;
            if (Auto_Detect.Checked==false&&(Clusters_No > li.Count || Clusters_No == 0))
                MessageBox.Show(" Please Enter a valid Number of cluster .");
            before = System.Environment.TickCount; // get the time before the operations are done
            RGBPixel[, ,] grid = RGB.Clone() as RGBPixel[, ,]; // Get Clone  to Save True 3dRGP
            quan = new MST(ref li, Clusters_No, grid, Auto_Detect.Checked);
            quan.GetClusters();
            if (Auto_Detect.Checked == true)
                No_of_clust.Text = quan.Pallete.Count.ToString();
                       
            ImageOperations.DisplayImage(ImageMatrix, pictureBox2, grid);
            after = System.Environment.TickCount; // get the time after the operations are done
            textBox2.Text = (quan as MST).MST_Value.ToString();
            double total = after - before;             // Calculate the taken time
            total /= 1000;                            // convert miliseconds to minutes
            total = Math.Round(total, 3);               // Round the Minutes to three decimal digits
            MessageBox.Show("Time Elapsed : " + total.ToString() + " seconds (s)");// show the taken Time
        
        }

        /// <summary>
        /// Get the distinct colors from the colored image matrix 
        /// </summary>
        /// <param name="arrayOfColors">2D array of colors</param>
        /// <returns>array of distinct colors</returns>
        /// The complexity of this function is : O(N^2)
        public static List<RGBPixel> getDistinct(RGBPixel[,] arrayOfColors)
        {
            List<RGBPixel> q = new List<RGBPixel>(); // complexity : O(1)
            bool[, ,] Histogram = new bool[256, 256, 256];  // complexity : O(1)
            RGBPixel False_cmp = new RGBPixel(255, 255, 255);// complexity : O(1)
           
            for (int i = 0; i < 256; i++) //-----------------------------------------------|
                for (int j = 0; j < 256; j++)//                                            |
                    for (int k = 0; k < 256; k++)//                                        |
                    {//                                                                    |Set All Color False (255 , 255 , 255)
                        RGB[i, j, k].red = 255; //O(1)                                     |Complexity is O(1) as it loops a constant no of iterations
                        RGB[i, j, k].green = 255;//O(1)                                    |
                        RGB[i, j, k].blue = 255;//O(1)                                     |
                    }//--------------------------------------------------------------------|
            for (int i = 0; i < arrayOfColors.GetLength(0); i++) //-------------------------------------------------------------------------------------------------------|
            {//                                                                                                                                                           |
                for (int j = 0; j < arrayOfColors.GetLength(1); j++)//-----------------------------------------------------------|                                        |
                {//                                                                                                              |                                        |
                    //                                                                                                           |                                        |
                    if (Histogram[arrayOfColors[i, j].red, arrayOfColors[i, j].green, arrayOfColors[i, j].blue] == true)// O(1). |                                        |
                        continue;// O(1).                                                                                        |                                        | 
                    //                                                                                                           |                                        |
                    if (RGB[arrayOfColors[i, j].red, arrayOfColors[i, j].green, arrayOfColors[i, j].blue] == False_cmp)//        |The complexity of the the loop is :     |The complexity of the the loop is :
                    {//                                                                                                          |No. of iteration x order of the body    |No. of iteration x order of the body
                        RGB[arrayOfColors[i, j].red, arrayOfColors[i, j].green, arrayOfColors[i, j].blue] =//                    |So,the complexity = N x O(1) = O(N).    |So,the complexity = N x O(N) = O(N^2).  
                             new RGBPixel(arrayOfColors[i, j].red, arrayOfColors[i, j].green, arrayOfColors[i, j].blue);//       |                                        |
                        q.Add(arrayOfColors[i, j]);// O(1).                                                                      |                                        | 
                    } //                                                                                                         |                                        |
                                         //                                                                                      |                                        |                                         
                    Histogram[arrayOfColors[i, j].red, arrayOfColors[i, j].green, arrayOfColors[i, j].blue] = true;// O(1).      |                                        |
                }//--------------------------------------------------------------------------------------------------------------|                                        |
            } //----------------------------------------------------------------------------------------------------------------------------------------------------------|


            return q; // O(1).
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (p != null) p.Close();
            if (quan != null && quan.Pallete.Count != 0)
            {
                p = new ShowPallete(ref quan.Pallete);
                p.Show();
            }

            else
                MessageBox.Show("Please Select an Image .");
        }
       
    }
}