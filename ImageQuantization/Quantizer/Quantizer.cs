﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImageQuantization.Quantizer
{
    public class Quantizer
    {
        protected int NO_Cluster;    // Number of CLuster
        protected List<RGBPixel> li;       // Distinct Color
        protected RGBPixel[, ,] RGB;       // 3D Array to Change Values of Colors 
        public List<RGBPixel> Pallete; // save new Color to show It

        /// <summary>
        /// Just Constrauctor 
        /// </summary>
        public Quantizer()
        {

        }

        /// <summary>
        /// Intailize member Varaible 
        /// </summary>
        /// <param name="NOCluster">Number of CLuster</param>
        /// <param name="li">Distinct Color</param>
        /// <param name="Grid">3D Array To change values of Colors</param>
        public Quantizer(int NOCluster, List<RGBPixel> li, RGBPixel[, ,] Grid) // x -> #cluster
        {
            this.RGB = Grid;
            this.li = li;
            this.NO_Cluster = NOCluster;
            Pallete = new List<RGBPixel>(NO_Cluster);

        }
        /// <summary>
        /// Holds the values of verticies of the graph : the index of the vertex "vertex", the weight of the vertex "Key"
        /// </summary>
        public struct node
        {
            public int vertex;
            public double key;
        }
        /// <summary>
        /// Calculates the euclidean distance between two distinct colors 
        /// </summary>
        /// <param name="RGB1">First distinct color</param>
        /// <param name="RGB2">Second distinct color</param>
        /// <returns>The calculated euclidean distance</returns>
        /// The complexity of this function is : O(1)
        public static double cal_eculdian_dist(RGBPixel RGB1, RGBPixel RGB2)
        {
            double result1 = (RGB1.red - RGB2.red) * (RGB1.red - RGB2.red);// The Complexity is : O(1).
            double result2 = (RGB1.green - RGB2.green) * (RGB1.green - RGB2.green);// The Complexity is : O(1).
            double result3 = (RGB1.blue - RGB2.blue) * (RGB1.blue - RGB2.blue);// The Complexity is : O(1).
            return Math.Sqrt(result1 + result2 + result3);// The Complexity is : O(1).
        }
        /// <summary>
        /// Swaps two items in the priority queue to make the min item is the last item in the queue 
        /// </summary>
        /// <param name="pq">The priority queue</param>
        /// <param name="fpos">the postion of the first item</param>
        /// <param name="spos">the postion of the second item</param>
        /// The complexity of this function is : O(1).
        public static void swap(node[] pq, int fpos, int spos)
        {
            double tmp1;// The Complexity is : O(1).
            int tmp2;// The Complexity is : O(1).
            tmp1 = pq[fpos].key;// The Complexity is : O(1).
            tmp2 = pq[fpos].vertex;// The Complexity is : O(1).
            pq[fpos].key = pq[spos].key;// The Complexity is : O(1).
            pq[fpos].vertex = pq[spos].vertex;// The Complexity is : O(1).
            pq[spos].key = tmp1;// The Complexity is : O(1).
            pq[spos].vertex = tmp2;// The Complexity is : O(1).

        }
        /// <summary>
        /// Virtual Fumction to overide in every inhert
        /// </summary>
        public virtual void GetClusters() { }
   
    }
}
