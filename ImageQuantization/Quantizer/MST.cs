﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ImageQuantization.Quantizer;
namespace ImageQuantization.Quantizer
{
    class MST : ImageQuantization.Quantizer.Quantizer 
    {
        private RGBPixel[] distinct; // distinct color array
        private List<bool> check;    // bool Array use in MST in ' Single Linkage '
        private List<Edge> path;            // MST
        private List<List<RGBPixel>> Clustering; // OutPutClusters
        private List<List<int>> AdjBfs;    // Bfs to put the childs in the same cluster of the parent 
        private List<int>  Big; // List which stores the parent and child of the max edges
        public bool Auto_detect_cluster = false; // if enable Auto Detect
        public double MST_Value = 0;// MST Cost
        private double previous = double.MaxValue;//Store the previous value of Standard deviation "BONUS"
        private int countr = 0;      // Auto Detect to get Number of clusters "BONUS"
        
        /// <summary>
        /// Don't DO Any thing Just Played :V
        /// </summary>
        /// <param ></param>
        public MST()
        {
        }

        /// <summary>
        /// Juct Intilization Base  ' Quantizer ' Elemntes
        /// </summary>
        /// <param ></param>
        public MST(ref List<RGBPixel> li, int clusters, RGBPixel[, ,] Grid, bool Auto_detect)
            : base(clusters, li, Grid)
        {
            Auto_detect_cluster = Auto_detect;
            Clustering = new List<List<RGBPixel>>();
            distinct = li.ToArray();
        }

        /// <summary>
        /// Creates the Minimum Spanning Tree from the distinct colors array 
        /// </summary>
        /// <param name="distinct">The array of the distinct colors</param>
        /// The complexity of this function is : O(V^2) = O(E) "where V is the number of verticies and E is No. of graph edges ".
        public override void GetClusters()
        {
            path = new List<Edge>(distinct.Length);
            node u;// The Complexity is : O(1).
            double ED = 0;// The Complexity is : O(1).
            int v = distinct.Length;// The Complexity is : O(1).
            double minKey = double.PositiveInfinity;// The Complexity is : O(1).
            int index = 0;// The Complexity is : O(1).
            double[] Min_edges = new double[v];//// The Complexity is : O(1).
            int[] parent = new int[v];// The Complexity is : O(1).
            node[] PQ = new node[v];// The Complexity is : O(1).
            
            Min_edges[0] = 0;// The Complexity is : O(1).
            parent[0] = 0;// The Complexity is : O(1).
            PQ[0].key = 0;// The Complexity is : O(1).
            PQ[0].vertex = 0;// The Complexity is : O(1).
            for (int i = 1; i < v; i++)//------------------------------------------|This loop to initialize all verticies' weights with infinity and 
            {//                                                                    |their index like the index of distinct colors items' indicies              
                Min_edges[i] = double.PositiveInfinity;// The Complexity is : O(1).|
                PQ[i].key = double.PositiveInfinity;// The Complexity is : O(1).   |
                PQ[i].vertex = i;// The Complexity is : O(1).                      |The complexity of the the loop is :
                parent[i] = -1;// The Complexity is : O(1).                        |No. of iteration x order of the body
            }//--------------------------------------------------------------------|So,the complexity = V x O(1) = O(V).

            swap(PQ, 0, v - 1);// swapping the first vertex of the tree with the last item in the queue ----> The Complexity is : O(1). 
            int size = v - 1;// The Complexity is : O(1).
            while (size != -1)//------------------------------------------------------------------------------------------------------------------------------|
            {//                                                                                                                                               |
                minKey = double.PositiveInfinity;// The Complexity is : O(1).                                                                                 |
                index = 0;// The Complexity is : O(1).                                                                                                        |
                u = PQ[size];// The Complexity is : O(1).                                                                                                     |
                size--;// The Complexity is : O(1).                                                                                                           |
                //                                                                                                                                            |
                for (int i = 0; i <= size; i++)//------------------------------------------------------------------|                                          |
                {//                                                                                                |                                          |
                    ED = cal_eculdian_dist(distinct[u.vertex], distinct[PQ[i].vertex]);//The Complexity is : O(1). |                                          |
                    if (PQ[i].key > ED)//The Complexity is : O(1).                                                 |                                          |
                    {//                                                                                            |                                          |
                        PQ[i].key = ED;//The Complexity is : O(1).                                                 |                                          |
                        parent[PQ[i].vertex] = u.vertex;//The Complexity is : O(1).                                |The complexity of the the loop is :       |The complexity of the the loop is :
                    }//                                                                                            |No. of iteration x order of the body      |No. of iteration x order of the body
                    if (PQ[i].key < minKey)//The Complexity is : O(1).                                             |So,the complexity = V x O(1) = O(V).      |So,the complexity = V x O(V) = O(V^2).                      
                    {//                                                                                            |                                          |
                        minKey = PQ[i].key;//The Complexity is : O(1).                                             |                                          |
                        index = i;//The Complexity is : O(1).                                                      |                                          |
                    }//                                                                                            |                                          |
                }//------------------------------------------------------------------------------------------------|                                          | 
                Min_edges[++size] = u.key;// The Complexity is : O(1).                                                                                        |
                //                                                                                                                                            |
                //                                                                                                                                            |
                size--;//                                                                                                                                     |
                if (size == -1)// The Complexity is : O(1).                                                                                                   |
                {//                                                                                                                                           |
                    for (int i = parent.Length - 1; i >= 0; i--)//-------------------------------------------------------|                                    |
                    {//                                                                                                  |The complexity of the the loop is : |
                        path.Add(new Edge(parent[PQ[i].vertex], PQ[i].vertex, Min_edges[i]));//The Complexity is : O(1). |No. of iteration x order of the body|
                        MST_Value += Min_edges[i];//The Complexity is : O(1).                                            |So,the complexity = V x O(1) = O(V).|
                    }//--------------------------------------------------------------------------------------------------|                                    |
                    MST_Value = Math.Round(MST_Value, 2);// The Complexity is : O(1).                                                                         |
                }//                                                                                                                                           |
                if (size > 0)// The Complexity is : O(1).                                                                                                     |
                    swap(PQ, index, size);// swapping the minimum item with the last item in the queue ---> The Complexity is : O(1).                         |
            }//-----------------------------------------------------------------------------------------------------------------------------------------------|
         
        ///---------------------------BONUS SECTION------------------------------///  
            if (Auto_detect_cluster)//The Complexity is : O(1). 
            {
                NO_Cluster = 0;//The Complexity is : O(1) 
                getstandard(path);//The Complexity is : O(K x D)
                BeginCLustering();//The Complexity is : O(K x D)
            }
      ///---------------------------BONUS SECTION------------------------------/// 
            else BeginCLustering();//The Complexity is : O(K x D)
        }
        
        /// <summary>
        /// ---Apply Single Linkeage Clustering Algo ---------
        /// [1] Mark cost Max (k-1)  Edge in MST -1
        /// [2] loop on MST if cost != -1 "Not Max Edge " Bulid undirected graph adjbfs
        /// [3] Now We have CLusters in Adjbfs But childs of childs if Apply Bfs then,
        ///      we have a single CLuster foreach color Max in Big.
        /// </summary>
        /// The Complexity of this Fn is: O(K x D)
        private void BeginCLustering()
        {
            Clustering = new List<List<RGBPixel>>(); // The complexity is : O(1)
            path = path.OrderBy(Edge => Edge.cost).ToList();// Sort Quick Sort---> O(DlogD)

            check = Enumerable.Repeat(false, li.Count).ToList();  // Re Use to Check if Take it in Clustering or Not----->O(D)
            AdjBfs = new List<List<int>>(NO_Cluster);//Complexity is : O(1)
            for (int i = 0; i < li.Count; i++) // memset adjbfs----->O(D)
                AdjBfs.Add(new List<int>());//Complexity is : O(1)

            Big = new List<int>();//Complexity is : O(1)
            for (int i = 0; i < NO_Cluster - 1; i++) //------------------------------------------------------|
            {//                                                                                              |
                double max = -1;//Complexity is : O(1)                                                       |  
                int indx = 0;//Complexity is : O(1)                                                          |  
                for (int ii = 0; ii < path.Count; ii++)//------------|                                       |   
                {//                                                  |                                       |
                    if (max < path[ii].cost)//Complexity is : O(1)   |The complexity of the loop is:         |The complexity of the loop is: 
                    {//                                              |No. of iterations x O(Body)            |No. of iterations x O(Body)     
                        max = path[ii].cost;//Complexity is : O(1)   |So,the complexity is : D x O(1) = O(D).|So,the complexity is : K x O(D) = O(K x D).
                        indx = ii;//Complexity is : O(1)             |where D is the no. of distinct colors  |where D is the no. of distinct colors 
                    }//                                              |                                       |and K is the no. of clusters
                }//--------------------------------------------------|                                       |
                path[indx].cost = -1;//Complexity is : O(1)                                                  |
                Big.Add(path[indx].from);//Complexity is : O(1)                                              |
                Big.Add(path[indx].to);//Complexity is : O(1)                                                |
            }//----------------------------------------------------------------------------------------------|   


            for (int i = 0; i < path.Count; i++)//------------------------------|
            {//                                                                 |
                if (path[i].cost != -1)//Complexity is : O(1)                   |The complexity of the loop is:
                {//                                                             |No. of iterations x O(Body)
                    AdjBfs[path[i].from].Add(path[i].to);//Complexity is : O(1) |So,the complexity is : D x O(1) = O(D).
                    AdjBfs[path[i].to].Add(path[i].from);//Complexity is : O(1) |where D is the no. of distinct colors
                }//                                                             |
            }//-----------------------------------------------------------------|
            
            foreach (int i in Big)// O(K) x O(D) = O(K x D)
                if (!check[i])//Complexity is : O(1)   
                    Clustering.Add(new List<RGBPixel>(Clustering_BFS(i)));//The Complexity is : O(D)

            setClusters(); // Change Values o(D)
        }
        /// <summary>
        /// Give itTow indx in list of Disinct color return Cost
        /// </summary>
        /// <param return >Double Cost</param>
        /// The complexity of this Fn is : O(D).
        private List<RGBPixel> Clustering_BFS(int s)
        {

            List<RGBPixel> ret = new List<RGBPixel>();//Complexity is : O(1)
            ret.Add(li[s]); // add him in cluster---->//Complexity is : O(1)
            Queue<int> q = new Queue<int>();//Complexity is : O(1)
            q.Enqueue(s);//Complexity is : O(1)
            while (q.Count != 0) //----------------------------------------------------------------------|
            {//                                                                                          |
                int p = q.Dequeue();//Complexity is : O(1)                                               |
                if (!check[p])//Complexity is : O(1)                                                     |
                {//                                                                                      |
                    check[p] = true;   // Complexity is : O(1)                                           |The complexity of the code is :
                    for (int i = 0; i < AdjBfs[p].Count; i++) //no. of childs x O(1) = O(no. of childs)  |No. of iterations x O(Body)
                        if (!check[AdjBfs[p][i]])//Complexity is : O(1)                                  |So,the Complexity is :O(No. of childs)
                        {//                                                                              |as it's depending on how many child will 
                            ret.Add(li[AdjBfs[p][i]]); //Complexity is : O(1)                            |be enqueued in the queue
                            q.Enqueue(AdjBfs[p][i]);// Complexity is : O(1)                              |WORST CASE -----> O(D) 
                        }//                                                                              |where D is the No. of distinct colors
                }//                                                                                      |
            }//------------------------------------------------------------------------------------------|
            return ret;//Complexity is : O(1)
        }

        /// <summary>
        /// Get the representative color of each cluster
        /// </summary>
        /// <param name="cluster_Number">Cluster Index</param>
        /// <returns>Color Avrg</returns>
        /// The Complexity of this Fn is : O(Cluster.length) the length of 1 cluster
        private RGBPixel Avrge(int cluster_Number)
        {
            double R = 0, G = 0, B = 0; //Complexity is : O(1).

            for (int i = 0; i < Clustering[cluster_Number].Count; i++)//------------|
            {//                                                                     |
                R += Clustering[cluster_Number][i].red;//Complexity is : O(1).      |The Complexity of the loop = No. of iterations x complextiy of the body
                G += Clustering[cluster_Number][i].green;//Complexity is : O(1).    |So, the Complexity is : Cluster.length x O(1) = O(Cluster.length).
                B += Clustering[cluster_Number][i].blue;//Complexity is : O(1).     |
            }//---------------------------------------------------------------------|
            
            R /= Clustering[cluster_Number].Count;//Complexity is : O(1).
            G /= Clustering[cluster_Number].Count;//Complexity is : O(1).
            B /= Clustering[cluster_Number].Count;//Complexity is : O(1).
            
            return new RGBPixel(R, G, B);//Complexity is : O(1).
        }

        /// <summary>
        /// set color in 3d RGB volor Values
        /// </summary>
        /// The Complexity of this Fn is :O(D)
        private void setClusters()
        {
            for (int i = 0; i < Clustering.Count; i++) //-------------------------------------------------------------------|
            {//                                                                                                             |
                RGBPixel res = Avrge(i);//The Complexity is : O(Cluster[i].length)                                          |The Complexity of this loop is :No. of iterations x O(Body)
                Pallete.Add(res);//The Complexity is : O(1).                                                                |So, the complexity is : K x O(d) = O(D).
                for (int j = 0; j < Clustering[i].Count; j++)//The complexity is : O(Cluster[i].length)                     |where K is no. of clusters , d is the length of each cluster
                    RGB[Clustering[i][j].red, Clustering[i][j].green, Clustering[i][j].blue] = res;//The complexity is :O(1)|and D is the no. of distinct colors
            //                                                                                                              |
            }//-------------------------------------------------------------------------------------------------------------|
        }


        ///-------------------------BONUS FUNCTION--------------------///
        /// <summary>
        /// Auto detect the No. of Clusters and assign it to No_Culster to BeginClustering
        /// </summary> 
        /// <param name="path">the MST</param>
        /// <returns>No_Cluster</returns>
        /// The Complexity of this Fn is : O(K x D)
        private int getstandard(List<Edge> path)
        {
            double StanderDiv = 0, newstand, sum = 0, mean = 0;//The Complexity is : O(1)
            List<Edge> Tree = new List<Edge>(path);//The Complexity is : O(1)
            double maxcost = -1;//The Complexity is : O(1)
            int maxindx = 0;//The Complexity is : O(1)
            double maxdiff = 0;//The Complexity is : O(1)

            for (int i = 0; i < path.Count; i++)//The Complexity is : O(D)
                mean += path[i].cost;//The Complexity is : O(1)

            mean /= path.Count;//The Complexity is : O(1)

            for (int i = 0; i < path.Count; i++)
            {//--------------------------------------------------------|
                sum = mean - path[i].cost;//The Complexity is : O(1)   |
                sum *= sum;//The Complexity is : O(1)                  |The Complexity is : D x O(1) = O(D)
                StanderDiv += sum;//The Complexity is : O(1)           |
            }//--------------------------------------------------------|
            StanderDiv = Math.Sqrt(StanderDiv / path.Count);//The Complexity is : O(1)

            newstand = StanderDiv;//The Complexity is : O(1)

            for (int i = 0; i < path.Count; i++)//--------------------------------|
            {//                                                                   |
                maxdiff = Math.Abs(mean - path[i].cost);//The Complexity is : O(1)|
                if (maxcost < maxdiff)//The Complexity is : O(1)                  |
                {//                                                               |The Complexity is : D x O(1) = O(D)
                    maxcost = maxdiff;//The Complexity is : O(1)                  |
                    maxindx = i;//The Complexity is : O(1)                        |
                }//                                                               |
            }//-------------------------------------------------------------------|
            Tree.RemoveAt(maxindx);//The Complexity is : O(1)

            countr++;//The Complexity is : O(1)

            if ((Math.Abs(previous - newstand) <= 0.0001) || Tree.Count == 0)//The Complexity is : O(1)
                return NO_Cluster = countr;//The Complexity is : O(1)

            previous = newstand;//The Complexity is : O(1)

            getstandard(Tree);//The Complexity is : No. of iterations x O(Fn) = K x O(D) = O(K x D)

            countr = 0;//The Complexity is : O(1)
            return NO_Cluster;//The Complexity is : O(1)
        }
    }
}
